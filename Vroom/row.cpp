#include <malloc.h>
#include "row.h" 

Row::Row(T_RESULT *res, T_ROW row) { 
	fields = row; 
	result = res; 
} 

Row::~Row() { 
	if( !IsClosed() ) { 
		Close(); 
	} 
} 

void Row::Close() { 
	if( IsClosed() ) { 
		throw "Row closed."; 
	} 
	fields = (T_ROW)NULL; 
	result = (T_RESULT *)NULL; 
} 
int Row::GetFieldCount() { 
	if( IsClosed() ) { 
		throw "Row closed."; 
	} 
#if defined(HAS_MSQL) 
	return msqlNumFields(result); 
#elif defined(HAS_MYSQL) 
	return mysql_num_fields(result); 
#else 
	#error No database linked. 
#endif 
} 
// Caller should be prepared for a possible NULL 
// return value from this method. 
char *Row::GetField(int field) { 
	if( IsClosed() ) { 
		throw "Row closed."; 
	} 
	if( field < 1 || field > GetFieldCount() ) { 
		throw "Field index out of bounds."; 
	} 
	return fields[field-1]; 
} 
int Row::IsClosed() { 
	return (fields == (T_ROW)NULL); 
} 