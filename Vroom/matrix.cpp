#include "matrix.h"


namespace vroom {

	// Template-ul dimensiunii vectorului.
	template <class T> Line<T>::Line(std::size_t n) : parent(n) {
	}

	// Template-ul unei linii a matricei ca vector.
	template <class T> Line<T>::Line(std::initializer_list<T> l) : parent(l) {
	}

	// Template-ul dimensiunii matricei.
	template <class T> Matrix<T>::Matrix(std::size_t n) : parent(n, Line<T>(n)) {
	}

	// Template-ul matricei.
	template <class T> Matrix<T>::Matrix() : Matrix(0) {
	}

	// Template-ul matricei ca un set de linii.
	template <class T> Matrix<T>::Matrix(std::initializer_list<Line<T>> l) : parent(l) {
	}

	// Template-ul valorilor matricei.
	template <class T> Matrix<T> Matrix<T>::get_sub_matrix(const std::vector<Index>& indices) const {
		Matrix<T> sub_matrix(indices.size());
		for (std::size_t i = 0; i < indices.size(); ++i) {
			for (std::size_t j = 0; j < indices.size(); ++j) {
				sub_matrix[i][j] = (*this)[indices[i]][indices[j]];
			}
		}
		return sub_matrix;
	}

	template class Line<Cost>;
	template class Matrix<Cost>;

} // namespace vroom