#ifndef KRUSKAL_H
#define KRUSKAL_H

#include "structures/generic/undirected_graph.h"


namespace vroom {
	namespace utils {

		template <class T>
		UndirectedGraph<T> minimum_spanning_tree(const UndirectedGraph<T>& graph);

	} // namespace utils
} // namespace vroom

#endif