#include "connection.h" 

// Connecting the database
Connection::Connection(char *host, char *db) { 
#if defined(HAS_MSQL) 
	connection = -1; 
#elif defined(HAS_MYSQL) 
	connection = (MYSQL *)NULL; 
#else 
	#error No database linked. 
#endif 
	Connect(host, db, (char *)NULL, (char *)NULL); 
} 

Connection::Connection(char *host, char *db, char *uid, char *pw) { 
#if defined(HAS_MSQL) 
	connection = -1; 
#elif defined(HAS_MYSQL) 
	connection = (MYSQL *)NULL; 
#else 
	#error No database linked. 
#endif 
	Connect(host, db, uid, pw); 
} 

void Connection::Connect(char *host, char *db, char *uid, char *pw) { 
	int state; 
	if( IsConnected() ) { 
		throw "Connection has already been established."; 
	} 
#if defined(HAS_MSQL) 
	connection = msqlConnect(host); 
	state = msqlSelectDB(connection, db); 
#elif defined (HAS_MYSQL) 
	mysql_init(&mysql); 
	connection = mysql_real_connect(&mysql, host, uid, pw, db, 0, 0); 
#else 
	#error No database linked. 
#endif 
	if( !IsConnected() ) { 
		throw GetError(); 
	} 
	if( state < 0 ) { 
		throw GetError(); 
	} 
}

// Disconnecting the database
Connection::~Connection() { 
	if (IsConnected()) { 
		Close(); 
	} 
} 
void Connection::Close() { 
	if (!IsConnected()) { 
		return; 
	} 
#if defined(HAS_MSQL)msqlClose(connection); 
	connection = -1; 
#elif defined(HAS_MYSQL)mysql_close(connection); 
	connection = (MYSQL *)NULL; 
#else 
	#error No database linked. 
#endif 
}

// Querying the Database
Result *Connection::Query(char *sql) {
	T_RESULT *res; 
	int state; 
	
	// if not connectioned, there is nothing we can do 
	if( !IsConnected() ) { 
		throw "Not connected."; 
	} 
	
	// execute the query 
#if defined(HAS_MSQL) 
	state = msqlQuery(connection, sql); 
#elif defined(HAS_MYSQL) 
	state = mysql_query(connection, sql); 
#else 
	#error No database linked. 
#endif 
	// an error occurred 
	if( state < 0 ) { 
		throw GetError(); 
	} 
	// grab the result, if there was any 
#if defined(HAS_MSQL) 
	res = msqlStoreResult(); 
#elif defined(HAS_MYSQL) 
	res = mysql_store_result(connection); 
#else 
	#error No database linked. 
#endif 
	// if the result was null, it was an update or an error occurred 
	// NOTE: mSQL does not throw errors on msqlStoreResult() 
	if( res == (T_RESULT *)NULL ) { 
		// just set affected_rows to the return value from msqlQuery() 
#if defined(HAS_MSQL) 
		affected_rows = state; 
#elif defined(HAS_MYSQL) 
		// field_count != 0 means an error occurred 
		int field_count = mysql_num_fields(connection); 
		if( field_count != 0 ) { 
			throw GetError(); 
		} 
		else { 
			// store the affected_rows 
			affected_rows = mysql_affected_rows(connection); 
		} 
#else 
	#error No database linked. 
#endif 
		// return NULL for updates 
		return (Result *)NULL; 
	} 
	// return a Result instance for queries 
	return new Result(res); 
} 

// Reading errors
int Connection::GetAffectedRows() { 
	return affected_rows; 
} 
char *Connection::GetError() { 
#if defined(HAS_MSQL) 
	return msqlErrMsg; 
#elif defined(HAS_MYSQL) 
	if (IsConnected()) { 
		return mysql_error(connection); 
	} 
	else { 
		return mysql_error(&mysql); 
	} 
#else 
	#error No database linked. 
#endif 
} 
int Connection::IsConnected() {
#if defined(HAS_MSQL) 
	return !(connection < 0);
#elif defined(HAS_MYSQL) 
	return !(!connection);
#else 
#error No database linked. 
#endif 
}