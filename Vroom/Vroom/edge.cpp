#include <algorithm>

#include "structures/generic/edge.h"


namespace vroom {
	namespace utils {

		template <class T>
		// Defined by two vertices and a weight (the distance between the locations).
		Edge<T>::Edge(Index first_vertex, Index second_vertex, T weight)
			: _first_vertex(std::min(first_vertex, second_vertex)),
			_second_vertex(std::max(first_vertex, second_vertex)),
			_weight(weight) {
		}

		// Overloaded comparison operator for the vertices values.
		template <class T> bool Edge<T>::operator<(const Edge& rhs) const {
			return (this->_first_vertex < rhs._first_vertex) or
				((this->_first_vertex == rhs._first_vertex) and
				(this->_second_vertex < rhs._second_vertex));
		}
		
		// Overloaded equality operator for the vertices values.
		template <class T> bool Edge<T>::operator==(const Edge& rhs) const {
			return (this->_first_vertex == rhs._first_vertex) and
				(this->_second_vertex == rhs._second_vertex);
		}

		template class Edge<Cost>;

	} // namespace utils
} // namespace vroom
