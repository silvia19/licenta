#ifndef l_connection_h 
#define l_connection_h 

#include <sys/time.h>

#if defined(HAS_MSQL) 
#include <msql.h>
#elif defined(HAS_MYSQL) 
#include <mysql.h>
#endif 

#include "result.h" 

class Connection { 
private: 
	int affected_rows; 
#if defined(HAS_MSQL) 
	int connection; 
#elif defined(HAS_MYSQL) 
	MYSQL mysql; 
	MYSQL *connection; 
#else 
	#error No database defined. 
#endif 
public: 
	Connection(char *, char *); 
	Connection(char *, char *, char *, char *); 
	~Connection(); 
	
	void Close(); 
	void Connect(char *host, char *db, char *uid, char *pw); 
	int GetAffectedRows(); 
	char *GetError(); 
	int IsConnected(); 
	Result *Query(char *); 
}; 

#endif // l_connection_h docstore.mik.ua/orelly/linux/sql/ch13_02.htm