#ifndef l_result_h 
#define l_result_h 

#include <sys/time.h>

#if defined(HAS_MSQL) 
#include <msql.h>
#elif defined(HAS_MYSQL) 
#include <mysql.h>
#endif 

#include "row.h" 

class Result { 
private: 
	int row_count; 
	T_RESULT *result; 
	Row *current_row; 

public: 
	Result(T_RESULT *); 
	~Result(); 

	void Close(); 
	Row *GetCurrentRow(); 
	int GetRowCount(); 
	int Next(); 
}; 
#endif // l_result_h 

int Result::Next() { 
	T_ROW row; 
	if (result == (T_RESULT *)NULL) { 
		throw "Result set closed."; 
	} 
#if defined(HAS_MSQL)
	row = msqlFetchRow(result); 
#elif defined(HAS_MYSQL)
	row = mysql_fetch_row(result); 
#else 
	#error No database linked. 
#endif if (!row) { 
	current_row = (Row *)NULL; 
	return 0; 
	} 
	else { 
		current_row = new Row(result, row); 
		return 1; 
	} 
} 

Row *Result::GetCurrentRow() { 
	if (result == (T_RESULT *)NULL) { 
		throw "Result set closed."; 
	} 
	return current_row; 
} 

void Result::Close() { 
	if (result == (T_RESULT *)NULL) { 
		return; 
	} 
#if defined(HAS_MSQL)
	msqlFreeResult(result); 
#elif defined(HAS_MYSQL)
	mysql_free_result(result); 
#else 
	#error No database linked. 
#endif 
	result = (T_RESULT *)NULL; 
} 

int Result::GetRowCount() { 
	if (result == (T_RESULT *)NULL) { 
		throw "Result set closed."; 
	} 
	if (row_count > -1) { 
		return row_count; 
	} 
	else { 
	#if defined(HAS_MSQL)
		row_count = msqlNumRows(result); 
#elif defined(HAS_MYSQL)
		row_count = mysql_num_rows(result); 
#else 
	#error No database linked. 
#endif 
	return row_count; 
	} 
}