#include "algorithms/tsp/tsp.h"
#include "structures/generic/undirected_graph.h"


namespace vroom {

	TSP::TSP(const Input& input, std::vector<Index> job_ranks, Index vehicle_rank)
		: VRP(input),
		_vehicle_rank(vehicle_rank),
		_job_ranks(std::move(job_ranks)),
		_is_symmetric(true),
		_has_start(_input.vehicles[_vehicle_rank].has_start()),
		_has_end(_input.vehicles[_vehicle_rank].has_end()) {

		assert(!_job_ranks.empty());

		// Pick ranks to select from input matrix.
		std::vector<Index> matrix_ranks;
		std::transform(_job_ranks.cbegin(),
			_job_ranks.cend(),
			std::back_inserter(matrix_ranks),
			[&](const auto& r) { return _input.jobs[r].index(); });

		// Starting point of the route.
		if (_has_start) {
			// Adding start and remember rank in _matrix.
			_start = matrix_ranks.size();
			matrix_ranks.push_back(_input.vehicles[_vehicle_rank].start.value().index());
		}
		// End point of the route.
		if (_has_end) {
			// Adding end and remember rank in _matrix.
			if (_has_start and (_input.vehicles[_vehicle_rank].start.value().index() ==
				_input.vehicles[_vehicle_rank].end.value().index())) {
				// Avoiding duplicate for identical ranks.
				_end = _start;
			}
			else {
				_end = matrix_ranks.size();
				matrix_ranks.push_back(_input.vehicles[_vehicle_rank].end.value().index());
			}
		}

		_matrix = _input.get_sub_matrix(matrix_ranks);

		// No node will be matched with itself.
		for (Index i = 0; i < _matrix.size(); ++i) {
			_matrix[i][i] = INFINITE_COST;
		}

		// Starting point and final point locations coincide.
		_round_trip = _has_start and _has_end and (_start == _end);

		if (!_round_trip) {
			// Open tour cases.
			// Start point is given.
			if (_has_start and !_has_end) {
				// Forcing first location as start, end location decided during optimization.
				for (Index i = 0; i < _matrix.size(); ++i) {
					if (i != _start) {
						_matrix[i][_start] = 0;
					}
				}
			}
			// End point is given.
			if (!_has_start and _has_end) {
				// Forcing last location as end, start location decided during optimization.
				for (Index j = 0; j < _matrix.size(); ++j) {
					if (j != _end) {
						_matrix[_end][j] = 0;
					}
				}
			}
			// Both start point and end point are given.
			if (_has_start and _has_end) {
				// Forcing first location as start, last location as end to produce an open tour.
				assert(_start != _end);
				_matrix[_end][_start] = 0;
				for (Index j = 0; j < _matrix.size(); ++j) {
					if ((j != _start) and (j != _end)) {
						_matrix[_end][j] = INFINITE_COST;
					}
				}
			}
		}

	}

} // namespace vroom