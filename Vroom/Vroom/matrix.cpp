#include "structures/generic/matrix.h"


namespace vroom {

	// Size of the vector template.
	template <class T> Line<T>::Line(std::size_t n) : parent(n) {
	}

	// Line of the matrix as a vector template.
	template <class T> Line<T>::Line(std::initializer_list<T> l) : parent(l) {
	}

	// Matrix size template.
	template <class T> Matrix<T>::Matrix(std::size_t n) : parent(n, Line<T>(n)) {
	}

	// Matrix template.
	template <class T> Matrix<T>::Matrix() : Matrix(0) {
	}

	// Matrix as a set of lines template.
	template <class T> Matrix<T>::Matrix(std::initializer_list<Line<T>> l) : parent(l) {
	}

	// Matrix values template.
	template <class T> Matrix<T> Matrix<T>::get_sub_matrix(const std::vector<Index>& indices) const {
		Matrix<T> sub_matrix(indices.size());
		for (std::size_t i = 0; i < indices.size(); ++i) {
			for (std::size_t j = 0; j < indices.size(); ++j) {
				sub_matrix[i][j] = (*this)[indices[i]][indices[j]];
			}
		}
		return sub_matrix;
	}

	template class Line<Cost>;
	template class Matrix<Cost>;

} // namespace vroom