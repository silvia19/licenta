#ifndef EDGE_H
#define EDGE_H

#include "structures/typedefs.h"


namespace vroom {
	namespace utils {

		template <class T> class Edge {

		private:
			// Two vertices and the weight.
			Index _first_vertex;
			Index _second_vertex;
			T _weight;

		public:
			// Edge.
			Edge(Index first_vertex, Index second_vertex, T weight);

			// First vertex.
			Index get_first_vertex() const {
				return _first_vertex;
			};

			// Second vertex.
			Index get_second_vertex() const {
				return _second_vertex;
			};

			// Comparison operator.
			bool operator<(const Edge& rhs) const;

			// Equality operator.
			bool operator==(const Edge& rhs) const;

			// Weight.
			T get_weight() const {
				return _weight;
			};
		};

	} // namespace utils
} // namespace vroom

#endif