#include "exception.h"

namespace vroom {

	Exception::Exception(ERROR error, const std::string& message)
		: error(error), message(message) {
	}

} // namespace vroom