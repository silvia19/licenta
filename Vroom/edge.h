#ifndef EDGE_H
#define EDGE_H

#include "typedefs.h"


namespace vroom {
	namespace utils {

		template <class T> class Edge {

		private:
			// Cele doua noduri extremitati, costul si timpul.
			Index _first_vertex;
			Index _second_vertex;
			T _weight;
			T _time;

		public:
			// Muchia.
			Edge(Index first_vertex, Index second_vertex, T weight, T time);

			// Primul nod.
			Index get_first_vertex() const {
				return _first_vertex;
			};

			// Al doilea nod.
			Index get_second_vertex() const {
				return _second_vertex;
			};

			// Operatorul de comparatie.
			bool operator<(const Edge& rhs) const;

			// Operatorul de egalitate.
			bool operator==(const Edge& rhs) const;

			// Costul.
			T get_weight() const {
				return _weight;
			};

			// Timpul
			T get_time() const {
				return _weight;
			};
		};

	} // namespace utils
} // namespace vroom

#endif