#include <algorithm>
#include "edge.h"


namespace vroom {
	namespace utils {

		template <class T>
		// Definit de doua noduri, un cost (distanta) si o alocare de timp (pentru parcurgere).
		Edge<T>::Edge(Index first_vertex, Index second_vertex, T weight, T time)
			: _first_vertex(std::min(first_vertex, second_vertex)),
			_second_vertex(std::max(first_vertex, second_vertex)),
			_weight(weight),
			_time(time) {
		}

		// Operatorul de comparatie supraincarcat pentru valorile nodurilor.
		template <class T> bool Edge<T>::operator<(const Edge& rhs) const {
			return (this->_first_vertex < rhs._first_vertex) or
				((this->_first_vertex == rhs._first_vertex) and
				(this->_second_vertex < rhs._second_vertex));
		}
		
		// Operatorul de egalitate supraincarcat pentru valorile nodurilor.
		template <class T> bool Edge<T>::operator==(const Edge& rhs) const {
			return (this->_first_vertex == rhs._first_vertex) and
				(this->_second_vertex == rhs._second_vertex);
		}

		template class Edge<Cost>;

	} // namespace utils
} // namespace vroom
