#include <algorithm>
#include <numeric>
#include <vector>

#include "kruskal.h"
#include "edge.h"
#include "typedefs.h"


namespace vroom {
	namespace utils {

		template <class T>
		UndirectedGraph<T> minimum_spanning_tree(const UndirectedGraph<T>& graph) {

			// Edges from original graph.
			std::vector<Edge<T>> edges = graph.get_edges();

			// Sorting edges by weight.
			std::sort(edges.begin(), edges.end(), [](const auto& a, const auto& b) {
				return a.get_weight() < b.get_weight();
			});

			// Storing the edges of the minimum spanning tree.
			std::vector<Edge<T>> mst;

			// Defining the graph size and constantly increase the number of the
			// conneted components until they are all accessed.
			std::vector<Index> representative(graph.size());
			std::iota(representative.begin(), representative.end(), 0);

			for (const auto& edge : edges) {
				Index first_vertex = edge.get_first_vertex();
				Index second_vertex = edge.get_second_vertex();

				// The two vertices of the found edge.
				Index first_rep = representative[first_vertex];
				Index second_rep = representative[second_vertex];
				if (first_rep != second_rep) {
					// Vertices are in separate connected components (no cycles).
					mst.push_back(edge);

					// Both vertices are in the same connected component.
					for (auto& e : representative) {		// Automatically deducted variable type
						if (e == second_rep) {
							e = first_rep;
						}
					}
				}
			}

			// Finalized graph after algorithm.
			return UndirectedGraph<T>(mst);
		}

		template UndirectedGraph<Cost>
			minimum_spanning_tree(const UndirectedGraph<Cost>& graph);

	} // namespace utils
} // namespace vroom
