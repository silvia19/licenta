#ifndef MATRIX_H
#define MATRIX_H

#include <initializer_list>
#include <vector>

#include "typedefs.h"


namespace vroom {

	// Definirea unei linii a matricei ca un vector de valori.
	template <class T> class Line : private std::vector<T> {

		using parent = std::vector<T>;

	public:
		using parent::size;				// Dimensiunea vectorului.
		using parent::operator[];		// Operatorii vectorului.

		Line(std::size_t n);

		Line(std::initializer_list<T> l);
	};

	// Definirea matricei ca un set de vectori.
	template <class T> class Matrix : private std::vector<Line<T>> {

		using parent = std::vector<Line<T>>;

	public:
		using parent::size;
		using parent::operator[];

		Matrix();

		Matrix(std::size_t n);

		Matrix(std::initializer_list<Line<T>> l);

		Matrix<T> get_sub_matrix(const std::vector<Index>& indices) const;
	};

} // namespace vroom

#endif