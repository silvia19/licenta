#ifndef UNDIRECTED_GRAPH_H
#define UNDIRECTED_GRAPH_H

#include <list>
#include <unordered_map>
#include <vector>

#include "edge.h"
#include "matrix.h"


namespace vroom {
	namespace utils {

		template <class T> class UndirectedGraph {

		private:
			// The size of the graph.
			unsigned _size;

			// Embedding two representations for different uses depending on context.
			std::vector<Edge<T>> _edges;
			std::unordered_map<Index, std::vector<Index>> _adjacency_list;

		public:
			UndirectedGraph();

			UndirectedGraph(const Matrix<T>& m);

			UndirectedGraph(std::vector<Edge<T>> edges);

			// Size.
			std::size_t size() const;

			// Vector representation.
			std::vector<Edge<T>> get_edges() const;

			// Map representation.
			std::unordered_map<Index, std::list<Index>> get_adjacency_list() const;
		};

	} // namespace utils
} // namespace vroom

#endif