USE [RoutingApp]
GO

/****** Object:  Table [dbo].[TipOras]    Script Date: 5/10/2020 8:13:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TipOras](
	[id_tip_oras] [numeric](18, 0) NULL,
	[dimensiune] [real] NULL,
	[importanta] [nchar](10) NULL,
	[tip_oras] [nchar](10) NULL
) ON [PRIMARY]
GO

