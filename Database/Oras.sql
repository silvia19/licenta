USE [RoutingApp]
GO

/****** Object:  Table [dbo].[Oras]    Script Date: 5/10/2020 8:13:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Oras](
	[id_oras] [numeric](18, 0) NULL,
	[id_statie_incarcare] [numeric](18, 0) NULL,
	[id_preferinta] [numeric](18, 0) NULL,
	[id_tip_oras] [numeric](18, 0) NULL,
	[nume_oras] [nchar](10) NULL,
	[judet] [nchar](10) NULL
) ON [PRIMARY]
GO

