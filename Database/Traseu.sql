USE [RoutingApp]
GO

/****** Object:  Table [dbo].[Traseu]    Script Date: 5/10/2020 8:14:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Traseu](
	[id_traseu] [numeric](18, 0) NULL,
	[id_oras_plecare] [numeric](18, 0) NULL,
	[id_oras_destinatie] [numeric](18, 0) NULL,
	[id_eficienta] [numeric](18, 0) NULL,
	[id_statie] [numeric](18, 0) NULL,
	[id_ruta] [numeric](18, 0) NULL,
	[numar_rute] [numeric](18, 0) NULL
) ON [PRIMARY]
GO

