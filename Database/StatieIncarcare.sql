USE [RoutingApp]
GO

/****** Object:  Table [dbo].[StatieIncarcare]    Script Date: 5/10/2020 8:13:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StatieIncarcare](
	[id_statie] [numeric](18, 0) NULL,
	[id_oras] [numeric](18, 0) NULL,
	[tip_incarcator] [nchar](10) NULL,
	[durata_incarcare] [numeric](18, 0) NULL
) ON [PRIMARY]
GO

