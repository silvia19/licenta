USE [RoutingApp]
GO

/****** Object:  Table [dbo].[Preferinta]    Script Date: 5/10/2020 8:13:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Preferinta](
	[id_preferinta] [numeric](18, 0) NULL,
	[nume_oras] [nchar](10) NULL,
	[tip_oras] [nchar](10) NULL
) ON [PRIMARY]
GO

