USE [RoutingApp]
GO

/****** Object:  Table [dbo].[Ruta]    Script Date: 5/10/2020 8:13:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Ruta](
	[id_ruta] [numeric](18, 0) NULL,
	[id_oras_intermediar] [numeric](18, 0) NULL,
	[durata] [numeric](18, 0) NULL,
	[distanta] [numeric](18, 0) NULL
) ON [PRIMARY]
GO

