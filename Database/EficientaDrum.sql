USE [RoutingApp]
GO

/****** Object:  Table [dbo].[EficientaDrum]    Script Date: 5/10/2020 8:11:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EficientaDrum](
	[id_eficienta] [numeric](18, 0) NULL,
	[id_masina] [numeric](18, 0) NULL,
	[distanta] [numeric](18, 0) NULL,
	[timp] [numeric](18, 0) NULL,
	[consum] [real] NULL
) ON [PRIMARY]
GO

