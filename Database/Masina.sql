USE [RoutingApp]
GO

/****** Object:  Table [dbo].[Masina]    Script Date: 5/10/2020 8:12:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Masina](
	[id_masina] [numeric](18, 0) NULL,
	[marca] [nchar](10) NULL,
	[model] [nchar](10) NULL,
	[putere_motor] [numeric](18, 0) NULL,
	[capacitate_baterie] [nchar](10) NULL
) ON [PRIMARY]
GO

